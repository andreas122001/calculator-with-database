import { createRouter, createWebHistory } from 'vue-router'
import Home from '../views/Home.vue'
import ContactForm from '@/views/ContactForm.vue'
import About from '@/views/About.vue'
import LoginView from '@/views/LoginView'
import store from '@/store'

const routes = [
    {
        path: '/',
        redirect: Home
    },
    {
        path: '/form',
        name: 'ContactForm',
        component: ContactForm,
        meta: {
            reload: true
        }
    },
    {
        path: '/calculator',
        name: 'Home',
        component: Home,
        meta: {
            reload: true
        }
    },
    {
        path: '/about',
        name: 'About',
        component: About
    },
    {
        path: '/login',
        name: 'Login',
        component: LoginView
    }
]

const router = createRouter({
    history: createWebHistory(process.env.BASE_URL),
    routes
})

router.beforeEach(async (to, from, next) => {
    await store.restored

    console.log('token: ' + store.state.token)
    console.log('user: ' + JSON.stringify(store.state.user))

    if (!(store.state.token && store.state.user) && to.name !== 'Login') {
        // not logged in so redirect to login page with the return url
        // return next('/login')
        console.log(store.state.token)
        return next({ path: '/login', query: { returnUrl: to.path } })
    }
    return next()
})

export default router
