import { createStore } from 'vuex'
// import VuexPersistence from 'vuex-persist'
// import localForage from 'localforage'

// const vuexLocal = new VuexPersistence({
//     storage: localForage,
//     asyncStorage: true
// })

export default createStore({
    state: {
        user: null,
        token: null,
        equation: '',
        symbols: [],
        history: [],
        lastSymbol: '',
        ans: '',
        form: {},
        flashMessage: {
            msg: '',
            color: ''
        }
    },
    mutations: {
        SET_EQUATION(state, equ) {
            state.equation = equ
        },
        ADD_TO_EQUATION(state, symbol) {
            state.equation += symbol
        },
        REMOVE_ONE(state) {
            state.equation = state.equation.substring(
                0,
                state.equation.length - 1
            )
        },
        ADD_TO_HISTORY(state, equ) {
            state.history.unshift(equ)
        },
        SET_HISTORY(state, history) {
            state.history = history
        },
        SET_ANS(state, ans) {
            state.ans = ans
        },
        SET_FORM(state, form) {
            state.form = form
        },
        SET_FLASH_MESSAGE(state, flash) {
            state.flashMessage.msg = flash.msg
            state.flashMessage.color = flash.color
        },
        RESET_FLASH_MESSAGE(state) {
            state.flashMessage.msg = ''
            state.flashMessage.color = ''
        },
        SET_USER(state, user) {
            state.user = user
        }
    },
    actions: {
        clear({ commit }) {
            commit('SET_EQUATION', '')
        },
        setAns({ commit }, ans) {
            commit('SET_ANS', ans)
        },
        useAns({ commit }) {
            commit('ADD_TO_EQUATION', this.state.ans)
        },
        clearHistory({ commit }) {
            commit('SET_HISTORY', [])
        },
        setEquation({ commit }, equation) {
            commit('SET_EQUATION', equation)
            commit('ADD_TO_HISTORY', equation)
        },
        setForm({ commit }, form) {
            console.log(form)
            commit('SET_FORM', form)
        },
        displayFlashMessage({ commit }, flash) {
            commit('SET_FLASH_MESSAGE', flash)
        },
        logout({ commit }) {
            commit('SET_USER', null)
        }
    },
    modules: {}
    // plugins: [vuexLocal.plugin]
})
