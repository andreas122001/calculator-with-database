import axios from 'axios'

export function doCalculate(request) {
    return axios
        .post(
            'http://localhost:8085/api/users/' +
                request.username +
                '/calculations',
            request.equation,
            {
                headers: {
                    Authorization: 'Bearer ' + request.token
                }
            }
        )
        .then((response) => {
            console.log(response.data)
            return response.data
        })
}
