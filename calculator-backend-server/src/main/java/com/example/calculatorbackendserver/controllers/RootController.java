package com.example.calculatorbackendserver.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/")
public class RootController {

    @GetMapping
    public String getRoot() {
        return "getroot";
    }

    @PostMapping
    public String postRoot() {
        return "postroot";
    }

}
