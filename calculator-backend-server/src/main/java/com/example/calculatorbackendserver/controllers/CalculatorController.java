package com.example.calculatorbackendserver.controllers;

import com.example.calculatorbackendserver.model.CalculateRequest;
import com.example.calculatorbackendserver.model.CalculateResponse;
import com.example.calculatorbackendserver.services.CalculatorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

// ignore plz
@RestController
@RequestMapping(value = "/calculate")
@EnableAutoConfiguration
@CrossOrigin
public class CalculatorController {

    @Autowired
    CalculatorService calculatorService;

    @PostMapping(value = "", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(value = HttpStatus.CREATED)
    public CalculateResponse doCalculate(@RequestBody CalculateRequest expr) {
        return new CalculateResponse(calculatorService.doCalculate(expr.getExpr()));
    }

    @GetMapping
    public String get() {
        return "getCalculator";
    }

}
