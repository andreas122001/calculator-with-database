package com.example.calculatorbackendserver.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class CalculateRequest {

    private final String expr;

    @JsonCreator
    public CalculateRequest(@JsonProperty("expr") final String expr) {
        this.expr = expr;
    }

    public String getExpr() {
        return expr;
    }

}
