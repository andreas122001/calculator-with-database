package com.example.calculatorbackendserver.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Calculation {
    @Id @GeneratedValue(strategy = GenerationType.AUTO) @JsonIgnore
    private int calculationId;
    @ManyToOne(fetch=FetchType.LAZY, optional = false)
    @JoinColumn(name = "user_id", nullable = false)
    @JsonIgnore
    private User user;
    private String calculation;
    private String answer;

    @JsonCreator
    public Calculation(@JsonProperty("calculation") String calculation,
                       @JsonProperty("answer") String answer) {
        this.calculation = calculation;
        this.answer = answer;
    }
}
