package com.example.calculatorbackendserver.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class CalculateResponse {

    private final String ans;

    @JsonCreator
    public CalculateResponse(@JsonProperty("ans") final String ans) {
        this.ans = ans;
    }

    public String getAns() {
        return ans;
    }
}
