package com.example.calculatorbackendserver.repo;

import com.example.calculatorbackendserver.model.Role;
import com.example.calculatorbackendserver.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepo extends JpaRepository<Role, Integer> {
    Role findByName(String name);
}

