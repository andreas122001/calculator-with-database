package com.example.calculatorbackendserver.repo;

import com.example.calculatorbackendserver.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepo extends JpaRepository<User, Integer> {
    User findByUsername(String username);
}
