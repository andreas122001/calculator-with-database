package com.example.calculatorbackendserver.repo;

import com.example.calculatorbackendserver.model.Calculation;
import com.example.calculatorbackendserver.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CalculationRepo extends JpaRepository<Calculation, Integer> {
    Calculation getByCalculation(String calculation);
    List<Calculation> findByUser(User user);
    Calculation deleteByUser(User user);
}
