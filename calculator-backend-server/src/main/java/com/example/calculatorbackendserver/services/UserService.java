package com.example.calculatorbackendserver.services;

import com.example.calculatorbackendserver.model.Calculation;
import com.example.calculatorbackendserver.model.Role;
import com.example.calculatorbackendserver.model.User;
import org.springframework.stereotype.Service;

import java.util.List;

public interface UserService {
    User saveUser(User user);
    User getUser(String username);
    List<User> getUsers();
    Calculation addCalculationToUser(String username, String calculation);
    List<Calculation> getUserCalculations(String username);
}
