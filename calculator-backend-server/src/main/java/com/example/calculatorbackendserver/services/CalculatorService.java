package com.example.calculatorbackendserver.services;

import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.Stack;

@Service
public class CalculatorService {

    private static final String operators = "+-*/^()";
    private static final String valid = "0123456789()+-*/.^  ";


    public String doCalculate(String expr) {
        return calculate(expr);
    }

    public static String calculate(String expr) {
        if (expr.charAt(expr.length()-1) == '=')
            expr = expr.substring(0,expr.length()-1);
        for (char c : expr.toCharArray()) if (!valid.contains(c+"")){
            System.out.println(c);
            return "Sorry! I can not evaluate this expression...";
        }

        expr = expr.trim();
        expr = expr.replace(")(",")*(");

        System.out.println(expr);

        String result = "error?";
        try {
            result = calc(toPolish(expr));
        } catch (Exception e) {
            System.err.println("Some error");
        }

        return result;
    }

    private static boolean isNumber(String c) {
        return !operators.contains(c);
    }

    private static LinkedList<String> toPolish(String expression) {
        expression = "(" + expression + ")";
        LinkedList<String> exp = expressionToList(expression);
        Stack<String> stack = new Stack<>();
        LinkedList<String> output = new LinkedList<>();

        while (!exp.isEmpty()) {
            String val = exp.poll();
            if (isNumber(val)) {
                output.add(val);
            } else if (val.equals("(")) {
                stack.add(val);
            } else if (val.equals(")")) {
                while (!stack.peek().equals("("))
                    output.add(stack.pop());
                stack.pop();
            } else {
                if (!isNumber(stack.peek()))
                    while ((getPriority(val) < getPriority(stack.peek()))
                            || (getPriority(val) <= getPriority(stack.peek())
                            && val.equals("^")))
                        output.add(stack.pop());
                stack.add(val);
            }
        }
        while(!stack.empty()) output.add(stack.pop());
        return output;
    }


    private static String calc(LinkedList<String> list) {
        Stack<String> stack = new Stack<>();
        while (!list.isEmpty()) {
            String val = list.poll();
            if (operators.contains(val)) {
                String val2 = stack.pop();
                String val1 = stack.pop();
                stack.push(operate(val, val1, val2));
            } else {
                stack.push(val);
            }
        }
        return stack.pop();
    }
    private static String calc2(LinkedList<String> list) {
        int numbers = 0;
        Stack<String> stack = new Stack<>();
        while (!list.isEmpty()) {
            String val = list.poll();
            if (operators.contains(val)) {
                stack.push(val);
            } else {
                numbers++;
                if (numbers == 2) {
                    String val1 = stack.pop();
                    String op = stack.pop();
                    stack.push(operate(op, val1, val));
                    numbers--;
                } else stack.push(val);
            }
        }
        return stack.pop();
    }

    private static int getPriority(String in) {
        char c = in.charAt(0);
        if (c == '-' || c == '+')
            return 1;
        else if (c == '*' || c == '/')
            return 2;
        else if (c == '^')
            return 3;
        return 0;
    }

    public static String operate(String operator, String val1, String val2) {
        double v1 = Double.parseDouble(val1);
        double v2 = Double.parseDouble(val2);
        switch (operator) {
            case "+":
                return String.valueOf(v1 + v2);
            case "-":
                return String.valueOf(v1 - v2);
            case "*":
                return String.valueOf(v1 * v2);
            case "/":
                return String.valueOf(v1 / v2);
            case "^":
                return String.valueOf(Math.pow(v1,v2));
            default:
                return "NaN";
        }
    }

    private static LinkedList<String> expressionToList(String expr) {
        LinkedList<String> list = new LinkedList<>();
        StringBuilder temp = new StringBuilder();
        for (char c : expr.toCharArray()) {
            if (operators.contains(c+"")) {
                if (temp.length()>0) {
                    list.add(temp.toString());
                    temp = new StringBuilder();
                }
                list.add(c+"");
            } else {
                temp.append(c);
            }
        }
        if (temp.length() > 0) list.add(temp.toString());
        return list;
    }



}
