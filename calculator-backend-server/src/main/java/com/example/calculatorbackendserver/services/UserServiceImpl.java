package com.example.calculatorbackendserver.services;

import com.example.calculatorbackendserver.model.Calculation;
import com.example.calculatorbackendserver.model.Role;
import com.example.calculatorbackendserver.model.User;
import com.example.calculatorbackendserver.repo.CalculationRepo;
import com.example.calculatorbackendserver.repo.RoleRepo;
import com.example.calculatorbackendserver.repo.UserRepo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Service @RequiredArgsConstructor @Transactional @Slf4j
public class UserServiceImpl implements UserService, UserDetailsService {
    private final UserRepo userRepo;
    private final RoleRepo roleRepo;
    private final CalculationRepo calculationRepo;
    private final PasswordEncoder passwordEncoder;
    private final CalculatorService calculatorService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepo.findByUsername(username);
        if (user == null) {
            log.error("User {} not found in database", username);
            throw new UsernameNotFoundException("User "+username+" not found in database");
        } else {
            log.info("User {} found in database", username);
        }
        Collection<SimpleGrantedAuthority> authorities = new ArrayList<>();
        user.getRoles().forEach(role -> { authorities.add(new SimpleGrantedAuthority(role.getName())); });
        return new org.springframework.security.core.userdetails.User(user.getUsername(),user.getPassword(),authorities);
    }

    @Override
    public User saveUser(User user) {
        log.info("Saving new user {} to database", user.getUsername());
        Role role = roleRepo.findByName("USER");
        String pass = user.getPassword();
        String encoded = passwordEncoder.encode(pass);
        user.setPassword(encoded);
        user.getRoles().add(role);
        return userRepo.save(user);
    }

    @Override
    public User getUser(String username) {
        log.info("Fetching user {}", username);
        return userRepo.findByUsername(username);
    }

    @Override
    public List<User> getUsers() {
        log.info("Fetching all users");
        return userRepo.findAll();
    }

    @Override
    public Calculation addCalculationToUser(String username, String calculationString) {
        log.info("Adding calculation {} to user {}", calculationString, username);
        String answer = calculatorService.doCalculate(calculationString);
        log.info("Answer: " + answer);
        User user = userRepo.findByUsername(username);
        Calculation calculation = new Calculation(-1, user, calculationString, answer);
//        user.getCalculations().add(calculation);
        calculationRepo.save(calculation);
        return calculation;
    }

    @Override
    public List<Calculation> getUserCalculations(String username) {
        log.info("Fetching calculations for user {}", username);
        User user = userRepo.findByUsername(username);
        return calculationRepo.findByUser(user);
    }

}
