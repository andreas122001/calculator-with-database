package com.example.calculatorbackendserver;

import com.example.calculatorbackendserver.model.Role;
import com.example.calculatorbackendserver.model.User;
import com.example.calculatorbackendserver.services.UserService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.ArrayList;

@SpringBootApplication
public class CalculatorBackendServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(CalculatorBackendServerApplication.class, args);
    }

    @Bean
    PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }


    @Bean
    public WebMvcConfigurer corsConfig() {
        return new WebMvcConfigurer() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/**")
                        .allowedMethods("*").allowedOrigins("*");
            }
        };
    }

    @Bean
    CommandLineRunner run(UserService userService) {
        return args -> {
            userService.saveUser(new User(-1,"Alberto123", "123", new ArrayList<>(), new ArrayList<>()));
            userService.saveUser(new User(-1,"andrebw", "supersecret", new ArrayList<>(), new ArrayList<>()));
        };
    }
}
