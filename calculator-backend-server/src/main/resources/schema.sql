create table if not exists user (
    id int auto_increment primary key,
    name varchar(20) not null,
    password varchar(40) not null
);

create table if not exists role (
    id int primary key,
    name varchar(20) not null
);

create table if not exists calculation (
    id int primary key,
    calculation varchar(50) not null
);